﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMusicPlayer.Utils
{
    /// <summary>
    /// 全局变量
    /// </summary>
    class GlobalVariable
    {
        // D:\WPF Project\WPFMusicPlayer\MyMusicPlayer\bin\Debug\playlist.xml
        public static readonly string PLAYLIST_FULL_PATH = AppDomain.CurrentDomain.BaseDirectory + "playlist.property";
        // XML音量节点
        public static readonly string VOLUME_NODE = "volume";

    }
}
