﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Foundation;

namespace MyMusicPlayer.Applications.Services
{
    [Export(typeof(IShellService)), Export]
    internal class ShellService : Model, IShellService
    {
        private object shellView;
        private object playlistView;
        private object controlPanelView;


        public object ShellView
        {
            get { return shellView; }
            set { SetProperty(ref shellView, value); }
        }

        public object PlaylistView
        {
            get { return playlistView; }
            set { SetProperty(ref playlistView, value); }
        }
        
        public object ControlPanelView
        {
            get { return controlPanelView; }
            set { SetProperty(ref controlPanelView, value); }
        }

    }
}
