﻿using MyMusicPlayer.Domain;
using System.Waf.Applications;

namespace MyMusicPlayer.Applications.Views
{
    internal interface IPlaylistView : IView
    {
        void SetMultiMusicInfoIndex(MusicInfo musicInfo);
    }
}
