﻿using MyMusicPlayer.Applications.ViewModels;
using MyMusicPlayer.Applications.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MyMusicPlayer.Domain;
using MyMusicPlayer.Utils;

namespace MyMusicPlayer.Presentation.Views
{
    /// <summary>
    /// PlaylistView.xaml 的交互逻辑
    /// </summary>
    [Export(typeof(IControlPanelView))]
    public partial class ControlPanelView : UserControl, IControlPanelView
    {
        public ControlPanelView()
        {
            InitializeComponent();
        }

    }
}
