﻿using MyMusicPlayer.Applications.ViewModels;
using MyMusicPlayer.Applications.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MyMusicPlayer.Domain;
using MyMusicPlayer.Utils;

namespace MyMusicPlayer.Presentation.Views
{
    /// <summary>
    /// PlaylistView.xaml 的交互逻辑
    /// </summary>
    [Export(typeof(IPlaylistView))]
    public partial class PlaylistView : UserControl, IPlaylistView
    {
        public PlaylistView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 删除歌曲后修改UI显示，使之与数据层保持一致
        /// 从被删除Item的角标开始，之后的所有Item的Index属性值减一（往前挪一位）
        /// </summary>
        /// <param name="musicInfo"></param>
        public void SetMultiMusicInfoIndex(MusicInfo musicInfo)
        {
            int index = musicInfo.Index;
            List<Label> list = UIHelper.GetChildObjects<Label>(this.playList, "index");
            foreach (var label in list)
            {
                int num = int.Parse(label.Content.ToString());
                if (num >= index)
                {
                    label.Content = num - 1;
                }
            }
        }

        private void OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            PlaylistViewModel vm = this.DataContext as PlaylistViewModel;
            vm.MouseDoubleClick.Execute(sender);
        }

        // 右键菜单：播放音乐
        private void PlayMusicRightClick(object sender, RoutedEventArgs e)
        {
            PlaylistViewModel vm = this.DataContext as PlaylistViewModel;
            vm.PlayMusicRightClick.Execute(sender);
        }

        // 右键菜单：打开文件所在目录
        private void OpenFileRightClick(object sender, RoutedEventArgs e)
        {
            PlaylistViewModel vm = this.DataContext as PlaylistViewModel;
            vm.OpenFileRightClick.Execute(sender);
        }

        // 右键菜单：删除一首歌
        private void DeleteMusicRightClick(object sender, RoutedEventArgs e)
        {
            PlaylistViewModel vm = this.DataContext as PlaylistViewModel;
            vm.DeleteMusicRightClick.Execute(sender);
        }

        // 右键菜单：清空列表
        private void DeleteAllRightClick(object sender, RoutedEventArgs e)
        {
            PlaylistViewModel vm = this.DataContext as PlaylistViewModel;
            vm.DeleteAllRightClick.Execute(sender);
        }

    }
}
