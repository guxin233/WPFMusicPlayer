﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyMusicPlayer.Domain
{
    /// <summary>
    /// 音乐、歌曲对象
    /// </summary>
    public class MusicInfo
    {
        public string Id { get; set; }      // 唯一标识符，防止重复添加同一歌曲。生成规则是 Md5(Name + Artist + Album + Duration)
        public int Index { get; set; }      // 在播放列表中的角标序号
        public string Name { get; set; }    // 歌曲名
        public string Artist { get; set; }  // 艺术家，歌手名
        public string Album { get; set; }   // 专辑
        public int Duration { get; set; }   // 时长，秒数
        public string FullPath { get; set; }        // 在本地的全路径。保存到本地数据库中。
        public string DurationText { get; set; }    // 附加属性。时长，用于UI显示。格式为 02：34

    }
}
