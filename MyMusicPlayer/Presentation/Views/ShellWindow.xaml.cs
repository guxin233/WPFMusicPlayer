﻿using System;
using System.ComponentModel.Composition;
using System.Windows;
using MyMusicPlayer.Applications.Views;

namespace MyMusicPlayer.Presentation.Views
{
    [Export(typeof(IShellView))]
    public partial class ShellWindow : Window, IShellView
    {
        public ShellWindow()
        {
            InitializeComponent();
        }

    }
}
