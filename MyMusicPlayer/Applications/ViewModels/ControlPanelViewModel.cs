﻿using System.ComponentModel.Composition;
using System.Waf.Applications;
using MyMusicPlayer.Applications.Views;
using System.Collections.ObjectModel;
using MyMusicPlayer.Domain;
using System.Windows.Input;
using System;
using System.Windows;

namespace MyMusicPlayer.Applications.ViewModels
{
    [Export]
    internal class ControlPanelViewModel : ViewModel<IControlPanelView>
    {
        [ImportingConstructor]
        public ControlPanelViewModel(IControlPanelView view)
            : base(view)
        {
        }


        private ICommand previousCommand;   // 上一首
        public ICommand PreviousCommand
        {
            get { return previousCommand; }
            set { previousCommand = value; }
        }

        private ICommand playCommand;       // 播放
        public ICommand PlayCommand
        {
            get { return playCommand; }
            set { playCommand = value; }
        }

        private ICommand pauseCommand;       // 暂停
        public ICommand PauseCommand
        {
            get { return pauseCommand; }
            set { pauseCommand = value; }
        }

        private ICommand nextCommand;       // 下一首
        public ICommand NextCommand
        {
            get { return nextCommand; }
            set { nextCommand = value; }
        }

        private ICommand changeSpeakerStateCommand; // 切换扬声器的状态：是否静音
        public ICommand ChangeSpeakerStateCommand
        {
            get { return changeSpeakerStateCommand; }
            set { changeSpeakerStateCommand = value; }
        }

    }
}
