﻿using System.ComponentModel.Composition;
using System.Waf.Applications;
using MyMusicPlayer.Applications.Views;
using System.Collections.ObjectModel;
using MyMusicPlayer.Domain;
using System.Windows.Input;
using MyMusicPlayer.Applications.Services;

namespace MyMusicPlayer.Applications.ViewModels
{
    [Export]
    internal class ShellViewModel : ViewModel<IShellView>
    {
        private readonly IShellService shellService;
        private readonly DelegateCommand exitCommand;


        [ImportingConstructor]
        public ShellViewModel(IShellView view, IShellService shellService)
            : base(view)
        {
            this.shellService = shellService;
            exitCommand = new DelegateCommand(Close);
        }

        public string Title { get { return ApplicationInfo.ProductName; } }

        public ICommand ExitCommand { get { return exitCommand; } }

        public IShellService ShellService { get { return shellService; } }


        private ICommand openFileCommand;   // 弹窗，添加本地音乐文件
        public ICommand OpenFileCommand
        {
            get { return openFileCommand; }
            set { openFileCommand = value; }
        }

        private ICommand openFolderCommand; // 弹窗，添加文件夹下的所有音乐文件
        public ICommand OpenFolderCommand
        {
            get { return openFolderCommand; }
            set { openFolderCommand = value; }
        }
        


        #region 启动、退出软件
        public void Show()
        {
            ViewCore.Show();
        }

        private void Close()
        {
            ViewCore.Close();
        }
        #endregion
    }
}
