﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMusicPlayer.Utils
{
    /// <summary>
    /// 时间工具类
    /// </summary>
    class TimeHelper
    {
        /// <summary>
        /// 130 --> 2:10
        /// </summary>
        /// <param name="sec"></param>
        /// <returns></returns>
        public static string Second2MinuteSecond(int sec)
        {
            TimeSpan time = TimeSpan.FromSeconds(sec);
            return time.ToString(@"mm\:ss");
        }

    }
}
