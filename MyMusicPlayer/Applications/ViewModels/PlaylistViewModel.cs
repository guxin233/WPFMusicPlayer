﻿using System.ComponentModel.Composition;
using System.Waf.Applications;
using MyMusicPlayer.Applications.Views;
using System.Collections.ObjectModel;
using MyMusicPlayer.Domain;
using System.Windows.Input;
using System;

namespace MyMusicPlayer.Applications.ViewModels
{
    [Export]
    internal class PlaylistViewModel : ViewModel<IPlaylistView>
    {
        [ImportingConstructor]
        public PlaylistViewModel(IPlaylistView view)
            : base(view)
        {
            this.MusicList = new ObservableCollection<MusicInfo>();
        }


        private ObservableCollection<MusicInfo> musicList;  // 前台绑定，音乐列表
        public ObservableCollection<MusicInfo> MusicList
        {
            get { return musicList; }
            set { musicList = value; }
        }

        private ICommand likeCommand;   // 添加音乐到我的喜爱列表
        public ICommand LikeCommand
        {
            get { return likeCommand; }
            set { likeCommand = value; }
        }

        private ICommand removeCommand; // 从播放列表中移除
        public ICommand RemoveCommand
        {
            get { return removeCommand; }
            set { removeCommand = value; }
        }

        private ICommand mouseDoubleClick;  // 鼠标双击：选中并播放音乐
        public ICommand MouseDoubleClick
        {
            get { return mouseDoubleClick; }
            set { mouseDoubleClick = value; }
        }

        private ICommand playMusicRightClick;   // 右键菜单：播放音乐
        public ICommand PlayMusicRightClick
        {
            get { return playMusicRightClick; }
            set { playMusicRightClick = value; }
        }


        private ICommand openFileRightClick;    // 右键菜单：打开文件所在目录
        public ICommand OpenFileRightClick
        {
            get { return openFileRightClick; }
            set { openFileRightClick = value; }
        }

        private ICommand deleteMusicRightClick; // 右键菜单：删除一首歌
        public ICommand DeleteMusicRightClick
        {
            get { return deleteMusicRightClick; }
            set { deleteMusicRightClick = value; }
        }

        private ICommand deleteAllRightClick;   // 右键菜单：清空列表
        public ICommand DeleteAllRightClick
        {
            get { return deleteAllRightClick; }
            set { deleteAllRightClick = value; }
        }


        /// <summary>
        /// 删除歌曲后修改UI显示，使之与数据层保持一致
        /// 从被删除Item的角标开始，之后的所有Item的Index属性值减一（往前挪一位）
        /// </summary>
        /// <param name="musicInfo"></param>
        public void SetMultiMusicInfoIndexByDeleteItem(MusicInfo musicInfo)
        {
            ViewCore.SetMultiMusicInfoIndex(musicInfo);
        }
        
    }
}
