﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMusicPlayer.Applications.Services
{
    public interface IShellService : INotifyPropertyChanged
    {
        // 主界面
        object ShellView { get; }
        // 播放列表
        object PlaylistView { get; set; }
        // 控制面板
        object ControlPanelView { get; set; }
    }
}
