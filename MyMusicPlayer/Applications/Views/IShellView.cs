﻿using System.Waf.Applications;

namespace MyMusicPlayer.Applications.Views
{
    internal interface IShellView : IView
    {
        void Show();

        void Close();
    }
}
